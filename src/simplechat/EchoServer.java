package simplechat;

// This file contains material supporting section 3.7 of the textbook:
// "Object Oriented Software Engineering" and is issued under the open-source
// license found at www.lloseng.com

import java.io.*;
import java.util.*;

import ocsf.server.*;

/**
 * This class overrides some of the methods in the abstract
 * superclass in order to give more functionality to the server.
 *
 * @author Dr Timothy C. Lethbridge
 * @author Dr Robert Lagani&egrave;re
 * @author Fran&ccedil;ois B&eacute;langer
 * @author Paul Holden
 * @version July 2000
 */
public class EchoServer extends AbstractServer {
    //Class variables *************************************************

    /**
     * The default port to listen on.
     */
    final public static int DEFAULT_PORT = 5555;
    private HashMap<ConnectionToClient, String> name = new HashMap<ConnectionToClient, String>();
    private HashMap<String, String> password = new HashMap<String, String>();
    private ArrayList<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
    //Constructors ****************************************************

    /**
     * Constructs an instance of the echo server.
     *
     * @param port The port number to connect on.
     */
    public EchoServer(int port) {
        super(port);
    }

    protected void clientDisconnected(ConnectionToClient client) {
        System.out.println("Client disconnected");
        for(ConnectionToClient c : clients){
            try {
                c.sendToClient(name.get(client) + " has disconnected");
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    synchronized protected void clientException(
            ConnectionToClient client, Throwable exception) {
        clientDisconnected(client);
    }

    protected void clientConnected(ConnectionToClient client) {
        try {
            client.sendToClient("Enter username and password separated by a space: ");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void addUser(String name, String password){
        this.password.put(name, password);
    }



    //Instance methods ************************************************

    /**
     * This method handles any messages received from the client.
     *
     * @param msg    The message received from the client.
     * @param client The connection from which the message originated.
     */
    public void handleMessageFromClient
    (Object msg, ConnectionToClient client) {
        if(!name.containsKey(client)){
            if(msg != null){
                String[] upw = msg.toString().split(" ");
                if(this.password.get(upw[0]).equals(upw[1]) && !name.containsValue(upw[0])){
                    name.put(client, upw[0]);
                    clients.add(client);
                    try {
                        client.sendToClient("Authentication: success");
                        for(ConnectionToClient c : clients){
                            try {
                                c.sendToClient(name.get(client) + " has connected");
                            } catch (IOException e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                else{
                    try {
                        client.sendToClient("User already connected or invalid password");
                        client.close();
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }else{
            if (msg != null) {
                System.out.println("Message received: " + msg + " from " + client);
                for(ConnectionToClient c : clients){
                    try {
                        c.sendToClient("<" + new Date().toString() + "> " + name.get(client) + ": " + msg);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }
    }

    /**
     * This method overrides the one in the superclass.  Called
     * when the server starts listening for connections.
     */
    protected void serverStarted() {
        System.out.println
                ("Server listening for connections on port " + getPort());
    }

    /**
     * This method overrides the one in the superclass.  Called
     * when the server stops listening for connections.
     */
    protected void serverStopped() {
        System.out.println
                ("Server has stopped listening for connections.");
    }

    //Class methods ***************************************************

    /**
     * This method is responsible for the creation of
     * the server instance (there is no UI in this phase).
     */
    public static void main(String[] args) {
        int port = 0; //Port to listen on

        try {
            port = Integer.parseInt(args[0]); //Get port from command line
        } catch (Throwable t) {
            port = DEFAULT_PORT; //Set port to 5555
        }



        EchoServer sv = new EchoServer(port);
        sv.addUser("Rob", "123");
        sv.addUser("Jacko", "asd");
        sv.addUser("Djurre", "kaas");

        try {
            sv.listen(); //Start listening for connections
        } catch (Exception ex) {
            System.out.println("ERROR - Could not listen for clients!");
        }
    }
}
//End of EchoServer class
